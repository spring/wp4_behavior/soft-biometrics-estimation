#!/usr/bin/env python3
# -*- coding:utf-8 -*-

import os
import pathlib
import sys
from collections import deque
from glob import glob

import cv2
import numpy as np
import PIL
import ros_numpy
import rospy
import torch
import torch.nn.functional as F
from hri_msgs.msg import IdsList, IdsMatch, SoftBiometrics
from sensor_msgs.msg import Image
from torchvision import transforms as T

file_path = pathlib.Path(__file__).parent.absolute()
sys.path.append(str(file_path))
sys.path.append(str(file_path.parent.parent))

import models  # noqa
from mtcnn import MTCNN  # noqa


def get_network():
    state_dict_path = os.path.join(os.path.dirname(__file__), "integrated.pt")
    device = "cuda" if torch.cuda.is_available else "cpu"
    model = models.integrated(
        age=True, gender=True, pretrained=state_dict_path, device=device,
    )
    model.eval().to(device)
    model.classify = False
    return model


def fixed_image_standardization(image_tensor):
    processed_tensor = (image_tensor - 127.5) / 128.0
    return processed_tensor


class SoftBiometricsEstimation:
    def __init__(self, database_path="/data/persons"):
        """Initialize ros publisher, ros subscriber"""
        self.device = "cuda" if torch.cuda.is_available else "cpu"
        self.network = get_network()

        self.gallery_paths = glob(os.path.join(database_path, "*"))
        self.gallery_names = [
            os.path.splitext(os.path.basename(name))[0] for name in self.gallery_paths
        ]
        self._mtcnn = MTCNN(
            image_size=160,
            margin=14,
            selection_method="center_weighted_size",
            device=self.device,
            select_largest=True,
        )

        self.gallery_features = []
        if not self.empty_database:
            for i, path in enumerate(self.gallery_paths):
                image = cv2.imread(path)[..., ::-1]
                feature = self._extract_faces(image).squeeze()
                self.gallery_features.append(feature)
            self.gallery_features = torch.stack(self.gallery_features, dim=0)

        self.faces_tracked_sub = rospy.Subscriber(
            "/humans/faces/tracked", IdsList, self._tracked_id_callback,
        )
        self.candidate_pub = rospy.Publisher(
            "/humans/candidate/matches", IdsMatch, queue_size=10
        )

        self.face_topics = {}
        self.past_pub = {}
        self.mov_avg_estimates = {}

    @property
    def empty_database(self):
        return len(self.gallery_paths) == 0

    def _tracked_id_callback(self, ros_ids):
        """Callback function for soft-biometrics evaluation."""
        face_idxs = ros_ids.ids
        subscribed_idxs = self.face_topics.keys()
        known_ids = list(set(face_idxs) | set(subscribed_idxs))
        for idx in known_ids:
            if idx in face_idxs and idx not in subscribed_idxs:
                self.face_topics[idx] = {}
                self.face_topics[idx]["cropped"] = rospy.Subscriber(
                    f"/humans/faces/{idx}/cropped",
                    Image,
                    self._cropped_image_callback,
                    callback_args=(idx),
                )
            elif idx not in face_idxs and idx in subscribed_idxs:
                face_topic = self.face_topics.get(idx)
                if face_topic is not None:
                    face_topic["cropped"].unregister()
                    del self.face_topics[idx]

                past_pub = self.past_pub.get(idx)
                if past_pub is not None:
                    past_pub.unregister()
                    del self.past_pub[idx]
                    del self.mov_avg_estimates[idx]

    def _cropped_image_callback(self, ros_img, face_id, *args):
        image_np = ros_numpy.numpify(ros_img).astype("float32")
        trans = T.Compose([T.ToTensor(), T.Resize((160, 160))])
        image_np = fixed_image_standardization(trans(image_np))

        softbio_estimations = self.inference(image_np)
        embeddings = softbio_estimations.pop("encoded")
        softbio_msg = SoftBiometrics(**softbio_estimations)

        soft_bio_pub = self.past_pub.get(face_id)
        mov_avg_estimates = self.mov_avg_estimates.get(face_id)
        if soft_bio_pub is None:
            soft_bio_pub = rospy.Publisher(
                f"/humans/faces/{face_id}/soft_biometrics",
                SoftBiometrics,
                queue_size=10,
            )
            self.past_pub[face_id] = soft_bio_pub

            mov_avg_estimates = {}
            mov_avg_estimates["age"] = deque([], maxlen=15)
            mov_avg_estimates["gender"] = deque([], maxlen=15)
            self.mov_avg_estimates[face_id] = mov_avg_estimates

        # Update the values to consider the moving average.
        mov_avg_estimates["age"].append(softbio_msg.age)
        mov_avg_estimates["gender"].append(softbio_msg.gender)
        softbio_msg.age = round(
            sum(mov_avg_estimates["age"]) / len(mov_avg_estimates["age"])
        )
        softbio_msg.gender = round(
            sum(mov_avg_estimates["gender"]) / len(mov_avg_estimates["gender"])
        )
        soft_bio_pub.publish(softbio_msg)

        # Evaluate the similarity with the gallery.
        if embeddings is not None and not self.empty_database:
            for i, embedding in enumerate(embeddings):
                distances = (
                    (self.gallery_features - embedding.cuda())
                    .norm(dim=1)
                    .detach()
                    .cpu()
                )

                # Select the top-k smallest distances.
                k = min(len(distances), 10)
                min_dists, min_idxs = distances.topk(k, largest=False)

                # Evaluate the probabilities and get the names.
                # Probabilities are evaluated on exp(min_dists) to increase differences.
                probs = torch.nn.functional.softmin(torch.exp(min_dists)).tolist()
                names = [self.gallery_names[idx] for idx in min_idxs]

                # Publish possible candidates.
                for name, dist, prob in zip(names, min_dists, probs):
                    if prob > 0.2 and dist < 1.2:
                        candidate_msg = IdsMatch(
                            id1=str(face_id), id1_type=2, id2=name, confidence=prob
                        )
                        self.candidate_pub.publish(candidate_msg)

    def _extract_faces(self, frame, display=False):
        bboxes, _ = self._mtcnn.detect(frame)
        if len(bboxes) > 1:
            bboxes = np.array([bboxes[0]])

        faces = []
        trans = T.Compose([np.float32, T.ToTensor(), T.Resize((160, 160))])
        if bboxes is not None:
            for (x, y, x2, y2) in bboxes:
                x = int(x)
                y = int(y)
                x2 = int(x2)
                y2 = int(y2)
                try:
                    face = frame[y:y2, x:x2]
                    faces.append(fixed_image_standardization(trans(face)))
                except:
                    pass

        if len(faces) > 0:
            faces = torch.stack(faces, dim=0).cuda()
            outputs = self.network(faces)
            return outputs["encoded"]

        return None

    def inference(self, face):
        inputs = torch.Tensor(face).unsqueeze(0)
        inputs = inputs.to("cuda" if torch.cuda.is_available else "cpu")
        outputs = self.network(inputs)

        embedding = outputs["encoded"].detach().cpu()

        preds = F.softmax(outputs["age"].detach().cpu(), dim=-1)
        classes = np.arange(0, preds.shape[1])
        age = round((preds * classes).sum(axis=-1).item())
        age_confidence = -1

        preds = F.softmax(outputs["gender"].detach().cpu(), dim=-1)
        classes = np.arange(0, preds.shape[1])
        predicted_output = (preds * classes).sum(axis=-1)
        gender = predicted_output.item()
        gender = int(gender <= 0.45) + 1
        gender_confidence = -1

        return {
            "age": age,
            "age_confidence": age_confidence,
            "gender": gender,
            "gender_confidence": gender_confidence,
            "encoded": embedding,
        }


if __name__ == "__main__":
    rospy.init_node("soft_biometrics_estimation", anonymous=True)
    soft_biometrics = SoftBiometricsEstimation()
    try:
        rospy.spin()
    except KeyboardInterrupt:
        print("Soft Biometrics module")
