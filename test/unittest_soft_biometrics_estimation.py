#!/usr/bin/env python3
# -*- coding:utf-8 -*-
import pathlib
import sys

file_path = pathlib.Path(__file__).parent.absolute()
sys.path.append(str(file_path))
sys.path.append(str(file_path.parent.parent))
print(sys.path)

import unittest

# ROS libraries
import roslib
import rospy
from time import sleep

# Ros Messages
from sensor_msgs.msg import CompressedImage
from wp4_msgs.msg import BoxDetectionFrame, SoftBiometrics, SoftBiometricsFrame


class TestSoftBioEstimation(unittest.TestCase):

    soft_biometrics_estimation_ok = False

    def _callback(self, data):
        self.soft_biometrics_estimation_ok = True

    def test_if_mask_detection_publish(self):
        rospy.Subscriber("/vision_msgs/soft_biometrics", SoftBiometricsFrame, self._callback)

        counter = 0
        while not rospy.is_shutdown() and counter < 5 and (not self.soft_biometrics_estimation_ok):
            sleep(1)
            counter += 1

        self.assertTrue(self.soft_biometrics_estimation_ok)
        
    
if __name__ == "__main__":
    import rostest
    rospy.init_node("test_soft_bio_estimation", log_level=rospy.INFO)
    rostest.rosrun("soft-biometrics-estimation", "unittest_soft_biometrics_estimation", TestSoftBioEstimation)