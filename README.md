# Soft-Biometrics Estimation



## Docker

### Build

```sh
docker build --target compiler --tag wp4_soft_biometrics_estimation:TAG .
```

### Run

```sh
# Default run
docker run --rm -it --net host --env ROS_MASTER_URI=http://ari-Xc:11311 --gpus '"device=0"' wp4_soft_biometrics_estimation:TAG

# Run with basestation
docker run --rm -it --net host --env ROS_MASTER_URI=http://ari-Xc:11311 --gpus '"device=0"' wp4_soft_biometrics_estimation:TAG basestation
```

To run with the face recognition module, add the argument `-v /path/to/database:/data/persons` to mount a folder containing pictures of people.
Currently, the module supports at most one picture per person, stored in a common image format.
The id of the person matches the name of the file, e.g. `/path/to/database/Alessandro.jpg` has the person id `Alessandro`.

## ROS Topics

### Dependencies

 - Head front camera RGB images (from ARI or basestation)
 - /humans/faces/{id}/cropped (from Mask Detection)
 - /humans/faces/tracked (from Mask Detection)

### Published

 - /humans/faces/{id}/soft_biometrics
 - /humans/candidate/matches (if a database of people is provided)
